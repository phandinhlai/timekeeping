﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using zkemkeeper;

namespace SyncTimekeeping
{
    public partial class SyncTimekeepingServices : ServiceBase
    {
        private readonly string IPTimekeeper = System.Configuration.ConfigurationManager.AppSettings["IpTimekeeper"];
        private readonly string PortTimekeeper = System.Configuration.ConfigurationManager.AppSettings["PortTimekeeper"];
        private readonly string UsernameLogin = System.Configuration.ConfigurationManager.AppSettings["UserNameLogin"];
        private readonly string PasswordLogin = System.Configuration.ConfigurationManager.AppSettings["PasswordLogin"];
        private readonly string TimeInterval = System.Configuration.ConfigurationManager.AppSettings["TimerIntervalHour"];
        private readonly int SinceDays = int.Parse(System.Configuration.ConfigurationManager.AppSettings["SinceDays"]);
        private static readonly HttpClient client = new HttpClient();

        private CZKEMClass timeKeeper = null;
        //private Timer timer = null;
        public SyncTimekeepingServices()
        {
            timeKeeper = new CZKEMClass();
            InitializeComponent();
        }

        public async Task StartDebug()
        {
            int time = int.Parse(TimeInterval) * 1000 * 60 * 60;
            while (true)
            {
                try
                {
                    await Timer_Tick();
                    await Task.Delay(time);
                }
                catch (Exception ex)
                {
                    Utilities.WriteLogError("Lỗi thực thi: " + ex.Source + "; " + ex.Message);
                    break;
                }
            }
        }

        private async Task<string> GetLoginInfo()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var param = new StringContent(serializer.Serialize(new { username = UsernameLogin, password = PasswordLogin }),
                    System.Text.Encoding.UTF8, "application/json");

            var response = await client.PostAsync("/session/login", param);

            string content = "";
            if (response.IsSuccessStatusCode)
            {
                content = await response.Content.ReadAsStringAsync();
            }
            var result = JObject.Parse(content).Children<JProperty>().FirstOrDefault(x => x.Name == "result");
            var sessionKey = result.Value.FirstOrDefault(x => x.ToString().Contains("SessionKey")).First;

            return sessionKey.ToString();
        }

        private async Task<bool> AddTimekeeping(string fingID, DateTime ioTime, string ioMode, string ioNum, string ioSource, string sessionID)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", sessionID);

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            var param = new StringContent(serializer.Serialize(new
            {
                values = new object[] {
                                new { FieldID = "C00", FieldType = "INT", Value = fingID},
                                new { FieldID = "C01", FieldType = "DTE", Value = ioTime},
                                new { FieldID = "C02", FieldType = "STR", Value = ioMode},
                                new { FieldID = "C03", FieldType = "INT", Value = ioNum},
                                new { FieldID = "C04", FieldType = "STR", Value = ioSource},
                        },
                sessionId = sessionID,
                moduleInfo = new { ModuleID = "02A64", SubModule = "MAD" },
                conditions = "",
                data = ""
            }), System.Text.Encoding.UTF8, "application/json");

            var response = await client.PostAsync("/maintain/execute", param);

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                return true;
            }

            return false;
        }

        public async Task Timer_Tick()
        {
            try
            {
                Utilities.WriteLogError("Bắt đầu đọc dữ liệu chấm công.");

                if (client.BaseAddress == null)
                {
                    client.BaseAddress = new Uri("http://mis.qcloud.asia/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                }

                string sessionKey = await GetLoginInfo();

                if (ConnectDevice(timeKeeper, IPTimekeeper, int.Parse(PortTimekeeper)))
                {
                    Utilities.WriteLogError("Kết nối thành công, bắt đầu đọc dữ liệu.");
                    int num1 = 0, num2 = 0, num3 = 0, num4 = 0, num5 = 0, year = 0, month = 0, day = 0, hour = 0, minute = 0;
                    string str = "";

                    timeKeeper.EnableDevice(1, false);
                    timeKeeper.GetDeviceStatus(1, 6, ref num5);
                    if (timeKeeper.ReadGeneralLogData(1))
                    {
                        while (timeKeeper.SSR_GetGeneralLogData(1, out str, out num1, out num2, out year, out month, out day, out hour, out minute, out num3, ref num4))
                        {
                            DateTime dateTime = new DateTime(year, month, day, hour, minute, 0, DateTimeKind.Local);
                            int int32 = Convert.ToInt32(str);
                            if (dateTime >= DateTime.Now.AddDays(SinceDays*-1))
                            {
                                if (await AddTimekeeping(int32.ToString(), dateTime, num2.ToString(), "0", IPTimekeeper, sessionKey))
                                    Utilities.WriteLogError(String.Format("[Success] !!! Import: {0}, {1}, {2}, {3}, {4}", int32, dateTime, num2, 0, IPTimekeeper));
                                else
                                    Utilities.WriteLogError(String.Format("[Error] !!! Import: {0}, {1}, {2}, {3}, {4}", int32, dateTime, num2, 0, IPTimekeeper));
                                await Task.Delay(500);
                            }
                        }
                    }
                }
                else
                {
                    Utilities.WriteLogError("Kết nối không thành công");
                }

            }
            catch (Exception ex)
            {
                Utilities.WriteLogError("Lỗi kết nối máy chấm công: " + ex.Source + "; " + ex.ToString());
            }

            timeKeeper.Disconnect();
        }

        private bool ConnectDevice(CZKEMClass timeKeeper, string IPAddress, int Port)
        {
            try
            {
                if (timeKeeper.Connect_Net(IPAddress, Port))
                    return true;
                timeKeeper.Disconnect();
                return false;
            }
            catch (Exception ex)
            {
                Utilities.WriteLogError("Lỗi kết nối máy chấm công: " + ex.Source + "; " + ex.Message);
                return false;
            }
        }
    }
}

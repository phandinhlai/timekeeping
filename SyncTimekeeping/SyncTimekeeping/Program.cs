﻿namespace SyncTimekeeping
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG
            new SyncTimekeepingServices().StartDebug().Wait();
#else
        ServiceBase[] ServicesToRun;
        ServicesToRun = new ServiceBase[]
        {
            new SyncTimekeepingServices()
        };
        ServiceBase.Run(ServicesToRun);
#endif

        }
    }
}
